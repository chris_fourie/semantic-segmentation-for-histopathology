#!/usr/bin/env bash

# Bash script to download files from google drive larger than 100mb
wget 'https://f.mjh.nz/gdrivedl'
sudo chmod +x gdrivedl

# x training data (.hdf5) 
./gdrivedl Ka0XfEMiwgCYPdTI-vv6eUElOBnKFKQ2
gunzip camelyonpatch_level_2_split_train_x.h5.gz 
# y training data (.hdf5)
./gdrivedl 1269yhu3pZDP8UYFQs-NYs3FPwuK-nGSG
gunzip camelyonpatch_level_2_split_train_y.h5.gz
# meta data (.csv)
./gdrivedl 1XoaGG3ek26YLFvGzmkKeOz54INW0fruR
#training mask (.hdf5)
./gdrivedl 1g04te-mWB_GvM4TFyhw3xdzrV8xTXPJO
gunzip camelyonpatch_level_2_split_train_mask.h5.gz


# x testing data (.hdf5) 
./gdrivedl 1qV65ZqZvWzuIVthK8eVDhIwrbnsJdbg_
gunzip camelyonpatch_level_2_split_test_x.h5.gz 
# y testing data (.hdf5)
./gdrivedl 17BHrSrwWKjYsOgTMmoqrIjDy6Fa2o_gP
!gunzip camelyonpatch_level_2_split_test_y.h5.gz
# meta data (.csv)
./gdrivedl 19tj7fBlQQrd4DapCjhZrom_fA4QlHqN4
