import logging

import h5py
import torch
import torch.utils.data as utils

logging.getLogger().setLevel(logging.INFO)


def load(x_path: str, y_path: str, mask_path: str, hyper_params: dict, chunk: int) -> utils.dataloader:
    """
    Loads a data chunk  
    """

    num_samples = hyper_params["training_samples"]
    num_chunks = int(num_samples / hyper_params["chunk_size"])
    data_chunk_segment_1 = chunk * int((num_samples / num_chunks))
    data_chunk_segment_2 = int((chunk + 1) * (num_samples / num_chunks))

    x_arrays = []
    y_arrays = []
    mask_arrays = []

    ## training set ##
    with h5py.File(x_path, "r") as x:
        with h5py.File(y_path, "r") as y:
            with h5py.File(mask_path, "r") as mask:

                # hdf5 file details
                print("training set X: {0}".format(list(x.values())))
                logging.info("training set X: {0}".format(list(x.values())))
                logging.info("training set Y: {0}".format(list(y.values())))
                logging.info("training set mask: {0}".format(list(mask.values())))

                for img_id in range(data_chunk_segment_1, data_chunk_segment_2):
                    x_arrays.append(x["x"][img_id])
                    y_arrays.append(y["y"][img_id])
                    mask_arrays.append(mask["mask"][img_id])

    # convert arrays lists into PyTorch tensors
    x_tensor = torch.stack([torch.Tensor(i) for i in x_arrays])
    y_tensor = torch.stack([torch.Tensor(i) for i in y_arrays])
    mask_tensor = torch.stack([torch.Tensor(i) for i in mask_arrays])

    # change the tensors axes to work with the PyTorch conv-nets
    # N,C,H,W i.e. batch_num, channel_num(depth), height, width
    x_tensor = x_tensor.permute(0, 3, 2, 1)
    y_tensor = y_tensor.permute(0, 3, 2, 1)
    mask_tensor = mask_tensor.permute(0, 3, 2, 1)

    logging.info("x_tensor: {0}".format(x_tensor.shape))
    logging.info("y_tensor: {0}".format(y_tensor.shape))
    logging.info("mask_tensor: {0}".format(mask_tensor.shape))

    # PyTorch dataset
    dataset = utils.TensorDataset(x_tensor, y_tensor, mask_tensor)
    # PyTorch dataloader
    dataloader = utils.DataLoader(
        dataset, shuffle=True, batch_size=hyper_params["batch_size"]
    )

    return dataloader


### for testing ###
def main():
    import sys
    from pathlib import Path

    base_path = Path(__file__).resolve().parents[1]
    sys.path.insert(0, str(base_path))

    hyper_params = {
        "training_samples": 196608,
        "validation_samples": 32768,
        "test_samples": 32768,
        "batch_size": 50,
        "momentum": 0.9,
        "learning_rate": 0.001,
        "epochs": 5,
        "confusion_matrix": True,
        "network": "UNet",
        "chunk_size": 100,
    }

    chunk = 2

    x_path = "datasets/camelyonpatch_level_2_split_train_x.h5"
    y_path = "datasets/camelyonpatch_level_2_split_train_y.h5"
    mask_path = "datasets/camelyonpatch_level_2_split_train_mask.h5"

    dataloader = load(x_path, y_path, mask_path, hyper_params, chunk)

    print(dataloader)


if __name__ == "__main__":
    main()
