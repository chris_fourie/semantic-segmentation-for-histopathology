import logging

import torch
import torch.optim as optim 
import torch.nn as nn

logging.getLogger().setLevel(logging.INFO)

def train(hyper_params: dict, dataloader, model):
    
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    logging.info(device)

    criterion = nn.BCEWithLogitsLoss()
    optimizer = optim.Adam(model.parameters(), lr = hyper_params["learning_rate"])
    f = open('training_data.csv', "a+") 
    f.write("epoch, batch, training_loss, training_loss_total, tn, fp, fn, tp, accuracy, specificity, sensitivity, duration, epoch_flag \n ")

    for epoch in range(hyper_params["epochs"]):
        
        total_loss = 0
        count = 0
        # mask_con_tot = np.zeros((1,1))  # TODO should this stay?
        # prediction_con_tot = np.zeros((1,1)) # TODO should this stay?

        for batch in dataloader:
            f.write("{0},".format(epoch))
            f.write("{0},".format(count))
            # start = time.time() TODO should this stay? 

            x, y, mask = batch
            x = x.to(device)
            y = y.to(device)
            mask = mask.to(device)
            optimizer.zero_grad()  

            prediction, x1, x2, x3 = model(x)
            prediction = prediction.to(device)  

            # param_list = list(model.parameters()) # length of list = 74 
            # experiment.log_other("parameter",param_list) - not logging 

            m = prediction.shape[0]  # batch size
            prediction = prediction.resize(m, 96, 96).to(device)
            mask = mask.resize(m, 96, 96)

            loss = criterion(prediction, mask.type(torch.FloatTensor).to(device))
            loss.backward()
            optimizer.step()
            total_loss += loss.item()

            # experiment.log_metric("batch train_loss",loss.item())
            # experiment.log_metric("batch train_total_loss",total_loss)
            f.write("{0},".format(loss.item()))
            f.write("{0},".format(total_loss))
