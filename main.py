
from utils.dataloader import load
from networks.u_net import UNet
from experiment.train import train
import torch

# Device configuration
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

hyper_params = {
    "training_samples": 30,
    "validation_samples": 32768,
    "test_samples": 32768,
    "batch_size": 50,
    "momentum": 0.9,
    "learning_rate": 0.001,
    "epochs": 5,
    "confusion_matrix": True,
    "network": "UNet",
    "chunk_size": 10,
}

### TRAIN ###
x_path = "datasets/camelyonpatch_level_2_split_train_x.h5"
y_path = "datasets/camelyonpatch_level_2_split_train_y.h5"
mask_path = "datasets/camelyonpatch_level_2_split_train_mask.h5"

num_chunks = int(hyper_params["training_samples"] / hyper_params["chunk_size"])

for chunk in range(0, num_chunks):
    dataloader = load(x_path, y_path, mask_path, hyper_params, chunk)
    
    model = UNet(3,1) if (hyper_params["network"] == "UNet") else None
    model = model.to(device)

    train(hyper_params, dataloader, model)



